.. _thu-cuoi-tu-mi-ung:

Tử Mị Ưng
=========

Xuất hiện trong sự kiện `Thiên Long Địa Đồ <http://ttl3d.zing.vn/su-kien/thien-long-dia-do-thang-11/the-le-494.html>`_ (10/11/2015)
cùng :ref:`thu-cuoi-u-lam-hai-long`, :ref:`thu-cuoi-phi-thuy-ngu-gia`.

.. image:: /tlbb_static/hinh_anh/trang_bi/thu_cuoi/tu_mi_ung/hinh_anh.jpg
