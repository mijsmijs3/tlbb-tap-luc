Long Phụng Trình Tường
======================

* | Trang phục Giáng sinh ra mắt sau đợt bảo trì 2/12/2015 Tân Thiên Long 3D.
  | Song song còn có kiểu tóc mới Anh Tư.

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/long_phung_trinh_tuong/cap_doi_tao_dang.jpg

* Giá: 6000 KNB

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/long_phung_trinh_tuong/hinh_anh_vat_pham.jpg

* Một số màu nhuộm

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/long_phung_trinh_tuong/nhuom_mau_do.jpg


  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/long_phung_trinh_tuong/nhuom_mau_hong.jpg


  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/long_phung_trinh_tuong/nhuom_mau_trang.jpg
