Cuồng Chiến Thiên Hạ
====================

* Thế Tộc tan rã, chuyển hướng về Bang Hội
* Dự kiến phát hành: T4 5/8/2015

Keynote Gặp Mặt Offline Giới Thiệu Phiên Bản Cuồng Chiến Thiên Hạ Tại TP. HCM - Ngày 2/8/2015
---------------------------------------------------------------------------------------------

* Thiên Hoang Cổ Cảnh

  * | Thế Tộc tan rã, bản đồ 3 thành Quân Thiên - Triều Kinh - La Phù trở thành các bản đồ hoạt động mới
    | Các bản đồ lân cận trở thành bãi luyện công
  * Phụ bản Phụng Minh Vương Lăng được làm lại, xuất hiện các BOSS mới, cách thức hoạt động mới

* Bang Hội

  * Xuất hiện kiến trúc mới, giúp trao đổi vật phẩm KNB, trang bị mới bằng điểm cống hiến bang hội
  * Bản đồ luyện cấp ngay trong bang hội
  * Mở tiệc bang hội, thành viên tham dự nhận cống hiến bang hội
  * | Hoạt động bảo vệ bang hội hằng tuần
    | > Chọn 16 bang hội có điểm hoạt động cao nhất trong toàn Tân Thế Giới
    | > Thi đấu bắt cặp > Chung kết: Khuynh Nhất Thiên Hạ

* Nhân vật

  * Luyện đơn (tối đa 5 cấp), tăng (+) Thể lực vĩnh viễn (tối đa khoảng 2400 điểm)

* Trang bị

  * Điêu văn kép: 2 điêu văn khác màu trên cùng một số trang bị

* Ghi chép: LingYun - Giao Long 7