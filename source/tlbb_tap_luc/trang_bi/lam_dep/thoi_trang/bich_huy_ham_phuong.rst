.. _thoi-trang-bich-huy-ham-phuong:

Bích Hủy Hàm Phương
===================

* Xuất hiện trong sự kiện
  `Báo Danh Năng Động <http://ttl3d.zing.vn/su-kien/hiep-khach-giang-ho/bao-danh-nang-dong-440.html>`_ (9/9/2015)
  cùng thú cưỡi :ref:`thu-cuoi-bich-giap-huyen-quy`.

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/bich_huy_ham_phuong/hinh_anh.jpg