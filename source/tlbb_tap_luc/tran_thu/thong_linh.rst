Thông Linh Trân Thú
===================

Giao Diện Thông Linh Trân Thú
-----------------------------

Giao diện trân thú sẽ có thêm một bảng nữa về chức năng Thông Linh.

Bên trong được chia làm 4 mức (quyển) thành tựu theo cấp độ tăng dần: Chu Tước, Huyền Vũ, Thanh Long và Bạch Hổ là cao nhất.
Phải kích hoạt đủ các điểm trong một thành tựu để chuyển sang thành tựu kế tiếp.
Mỗi thành tựu gồm 29 điểm (huyết mạch), khi kích hoạt chúng sẽ gia tăng thêm thuộc tính trân thú.

.. image:: /tlbb_static/hinh_anh/tran_thu/giac_tinh/giac_tinh_01.jpg

Vật Phẩm Gia Tăng Điểm Thông Linh Tinh Túy
------------------------------------------

Để nâng huyết mạch cần tiêu hao điểm Thông Linh tinh túy (chắc giống chân nguyên đây!) điểm này nằm ở góc phải của giao diện Thông Linh.

Và sẽ có vật phẩm giúp gia tăng điểm Thông Linh tinh túy này, bao gồm các mức tăng 10, 50, 100 điểm.

.. image:: /tlbb_static/hinh_anh/tran_thu/giac_tinh/vat_pham_giac_tinh_10.jpg

Dựa vào các cụm từ "Bậc" và "Cấp" trong bản cập nhật game vừa rồi xem ra cũng có thể suy nghĩ ít nhiều về tính năng này.

Theo tìm hiểu thêm thì còn có sự xuất hiện của một số hoạt động, phụ bản mới với phần thưởng là vật phẩm trên:

* | Huyết chiến Nhạn Môn Quan ??
  | Đánh 3 BOSS, và mấy cái lồng gỗ!

  .. image:: /tlbb_static/hinh_anh/hoat_dong/huyet_chien_nhan_mon_quan.jpg

  * http://tl.changyou.com/data/bbs/mrhd_ymg.shtml
  * Video: http://tl.17173.com/content/2014-10-04/20141004095152316.shtml

* | Vận tiêu
  | Một ngày 3 lần, người chơi cấp 85 trở lên
  | Có 3 mức tiêu đồng-bạc-vàng yêu cầu đưa tiêu ở Phụng Minh Trấn do NPC Tiêu Phong giao thì phải, có cả người chơi khác cướp tiêu nữa!

  .. image:: /tlbb_static/hinh_anh/hoat_dong/van_tieu.jpg

* Giết 3 con quái vật ở Kính Hồ (Hỗn Giang Long, Xuất Động Giao, và Phiên Giang Thận chăng)


Mức Tăng Thuộc Tính Trân Thú Theo Thành Tựu
-------------------------------------------

Và đây là bảng ghi chép mức gia tăng 14 thuộc tính của trân thú dựa vào các mức thành tựu Thông Linh với mức gia tăng theo %.

==============  ======== ======== ========== =======
Tên thuộc tính  Chu Tước Huyền Vũ Thanh Long Bạch Hổ
==============  ======== ======== ========== =======
Sinh lực        14       22       44         60
Ngoại công      4        12       24         40
Nội công        4        12       24         40
Ngoại thủ       4        12       24         40
Nội thủ         4        12       24         40
Chính xác       4        12       24         40
Né tránh        4        12       24         40
Chí mạng        4        12       24         40
Kháng chí mạng  4        12       24         40
Cường lực       4        12       24         50
Thể lực         4        22       34         50
Nội lực         4        12       24         50
Trí lực         4        12       24         40
Thân pháp       4        12       24         40
==============  ======== ======== ========== =======

* Sinh lực và Thể lực được ưu ái với tỉ lệ % cao.
* Các chỉ số thuộc tính cơ bản như Cường lực, Nội lực cũng nhỉnh hơn so với đa số các thuộc tính khác ở mức cuối - Bạch Hổ.

Thông tin
---------

* Ghi chép: LingYun
* Ngày giờ: 10/07/2015
* Tham khảo, nguồn, trích dẫn: http://twjh.changyou.com/data/bbs/tlys.shtml
* Nội dung được truyền tải theo cách hiểu của người viết, có thể gặp sai sót. Khi trích dẫn xin đồng đạo giang hồ ghi rõ nguồn!
