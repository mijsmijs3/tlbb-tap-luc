Lục Dã Tiên Tung
================

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/luc_da_tien_tung/vat_pham.png
   :alt: Vật phẩm thời trang Lục Dã Tiên Tung

   Vật phẩm thời trang Lục Dã Tiên Tung

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/luc_da_tien_tung/hinh_anh.png
   :alt: Thời trang Lục Dã Tiên Tung rất đẹp, màu xanh lá chủ đạo, cổ và lai áo cách điệu rất mới mẻ

   Thời trang Lục Dã Tiên Tung rất đẹp, màu xanh lá chủ đạo, cổ và lai áo cách điệu rất mới mẻ.