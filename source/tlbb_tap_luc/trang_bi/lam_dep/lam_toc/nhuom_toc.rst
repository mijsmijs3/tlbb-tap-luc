Nhuộm Tóc
=========

* Đến NPC Hà Linh Chi - Lạc Dương (346, 270).

* Tiêu tốn thuốc nhuộm tóc - vật phẩm Nhiễm Phát Tễ.

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/lam_toc/nhuom_toc/nhiem_phat_te.png

* Lựa 3 giá trị màu theo hệ Đỏ-Xanh Lá-Xanh Dương (RGB), và độ sáng. Có thể nhập giá trị hoặc kéo thanh trượt, mỗi khi thay đổi giá trị, màu tóc sẽ thay đổi theo để xem trước.  

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/lam_toc/nhuom_toc/nhuom_toc.png