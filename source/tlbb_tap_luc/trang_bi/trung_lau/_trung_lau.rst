.. _trang-bi-trung-lau:

Trùng Lâu
=========

.. NOTE::
   | Ma Tôn Trùng Lâu có giá trị cao nhưng phải kể đến hai bảo vật giá trị nhất là: Đới và Liên.
   | Giá trị được đánh giá thông qua hiệu ứng, thuộc tính, giá trị sử dụng lâu dài và *giá sở hữu* của món trùng lâu.
   |
   | *Mẹo*: 
   |
   | * Thuộc tính tinh thông (công kích thuộc tính) trang bị của Trùng Lâu có thể cao gấp đôi so với trang bị cùng loại (hộ phù, giới chỉ, ..) có cùng cấp tinh thông.


.. toctree::

   trung_lau_doi
   trung_lau_lien
   trung_lau_giap
   trung_lau_khoi
   trung_lau_kien
   trung_lau_thu
   trung_lau_ngoa
   trung_lau_gioi
   trung_lau_trieu
   trung_lau_ngoc
   trung_lau_quy
