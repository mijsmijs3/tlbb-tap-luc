.. _thoi-trang-tu-phong-diep-anh:

Tử Phong Diệp Ảnh
=================

Nhân vật nam đem tặng hoặc được tặng vật phẩm 999 Đóa hồng sẽ sở hữu được thời trang này.

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/tu_phong_diep_anh_trung_quang_dao_dat/tu_phong_diep_anh.png
   :alt: Hình ảnh vật phẩm thời trang Tử Phong Diệp Ảnh

   Hình ảnh vật phẩm thời trang Tử Phong Diệp Ảnh, với màu tím tinh tế, cánh quat cặp ngang lưng nho nhỏ.

.. _thoi-trang-trung-quang-dao-dat:

Trừng Quang Dao Dật
===================

Nhân vật nữ đem tặng hoặc được tặng vật phẩm 999 Đóa hồng sẽ sở hữu được thời trang này.

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/tu_phong_diep_anh_trung_quang_dao_dat/trung_quang_dao_dat.png
   :alt: Hình ảnh vật phẩm thời trang Trừng Quang Dao Dật

   Hình ảnh vật phẩm thời trang Trừng Quang Dao Dật, với màu tím tinh tế, chút lãng mạn, có hình cánh quạt cắp vai ở sau rất hiếu kỳ.


.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/tu_phong_diep_anh_trung_quang_dao_dat/cap_doi_phia_truoc.png
   :alt: Hình ảnh nhìn từ phía trước của cặp thời trang Tử Phong Diệp Ảnh - Trừng Quang Dao Dật

   Hình ảnh nhìn từ phía trước của cặp thời trang Tử Phong Diệp Ảnh - Trừng Quang Dao Dật


.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/tu_phong_diep_anh_trung_quang_dao_dat/cap_doi_phia_sau.png 
   :alt: Hình ảnh nhìn từ phía sau của cặp thời trang Tử Phong Diệp Ảnh - Trừng Quang Dao Dật

   Hình ảnh nhìn từ phía sau của cặp thời trang Tử Phong Diệp Ảnh - Trừng Quang Dao Dật


Để sở hữu cặp thời trang này, tìm hiểu :ref:`danh-hieu-tinh-thanh-mai-khoi-tien-tu`.

