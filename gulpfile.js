var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

gulp.task('default', function() {
	console.log("hi");
	
});

gulp.task('images', function() {
	gulp.src('source/tlbb_static/hinh_anh/**')
	        .pipe(imagemin({
	        	progressive: true,
            	use: [pngquant()]
        }))
        .pipe(gulp.dest('dist/images'));
});

gulp.task('copy-images', function() {
	gulp.src('dist/images/**')
		.pipe(gulp.dest('source/tlbb_static/hinh_anh'))
})