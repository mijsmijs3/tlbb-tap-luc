.. _ky-nang-song:

Kỹ Năng Sống
============

Thông tin, kinh nghiệm, mẹo, sơ đồ phân bố tài nguyên các loại kỹ năng sống.

.. toctree::
   :maxdepth: 2   

   duoc
   khai_khoang
   trong_trot
   may_mac

Liên Quan Phụ Trợ Chế Tạo
-------------------------

.. toctree::
   :maxdepth: 2

   da_tao_do_tuong
   gia_cong_nguyen_lieu

.. _ky-nang-song-thang-cap-toi-da-truc-tiep:

--------------------------
Thăng Cấp 1 - 10 Trực tiếp
--------------------------

Tiêu hao vàng để thăng cấp trực tiếp, không cần độ thành thạo kỹ năng.

* | May Mặc
  | NPC Mộc Uyển Thanh - Đại Lý (111, 132)

* | Công Nghệ
  | NPC Nguyễn Tinh Trúc - Kính Hồ (108, 140)

* | Đúc
  | NPC Gia Luật Đại Thạch - Lạc Dương (215, 205)


---------------------------------
Thăng Cấp Bắt Buộc Trong Bang Hội
---------------------------------

Kỹ năng sống các cấp từ 1 - 5 có thể thăng tại người dạy kỹ năng ở các thành thị thông thương, từ cấp 6 trở đi phải được thăng cấp trong thành thị bang hội.
Tiêu hao vàng, độ cống hiến bang hội.

Tất cả các kỹ năng có thể học ở các thành thị Lạc Dương, Đại Lý, Tô Châu, Lâu Lan,.. đều có thể học và thăng cấp được trong thành thị bang hội.

* Đào khoáng
* Trồng trọt
* Câu cá
* Hái dược
* Chế dược
* Nấu nướng
* Dưỡng sinh pháp
* Dược lý
* ...

=== ==== =====================
Cấp Vàng Độ cống hiến bang hội
=== ==== =====================
6   6    100
7   12   250
8   25   500
9   51   1000
10  102  2000
=== ==== =====================
