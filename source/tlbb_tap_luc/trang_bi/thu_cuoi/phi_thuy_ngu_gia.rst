.. _thu-cuoi-phi-thuy-ngu-gia:

Phỉ Thúy Ngự Giá
================

Xuất hiện trong sự kiện `Thiên Long Địa Đồ <http://ttl3d.zing.vn/su-kien/thien-long-dia-do-thang-11/the-le-494.html>`_ (10/11/2015)
cùng :ref:`thu-cuoi-u-lam-hai-long`, :ref:`thu-cuoi-tu-mi-ung`.

.. image:: /tlbb_static/hinh_anh/trang_bi/thu_cuoi/phi_thuy_ngu_gia/hinh_anh.jpg
