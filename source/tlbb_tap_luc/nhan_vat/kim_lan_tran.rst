Kim Lan Trận
============

* Là tính năng mới trong phiên bản Quân Lâm Thiên Hạ, thể hiện bởi bộ 4 kỹ năng giúp tăng cường sức mạnh cho nhân vật,
  để kích hoạt được phải có sự hợp tác với các bằng hữu kết bái, tối thiểu phải có 1 bằng hữu kết bái trực tuyến mới có thể khởi trận.
  Chỉ có thể kích hoạt một trận duy nhất tại một thời điểm.

* NPC liên quan: Trần Phu Chi (Lạc Dương); Nhấn phím ALT + S, chọn thẻ Trận để xem chi tiết.

* 4 loại trận pháp gồm:

  * Quy Nguyên Trận: tăng giới hạn sinh lực.
  * Phong Dương Trận: Tăng chính xác.
  * Thiên Phúc Trận: tăng sát thương xuyên thích.
  * Địa Tải Trận: miễn giảm sát thương xuyên thích.

* Đánh giá sức mạnh của trận được chia theo bậc và cấp.
  Có 10 cấp tối đa, mỗi cấp gồm 9 bậc, đạt tới bậc 9 của một cấp thì nhận nhiệm vu tiến cấp để lên cấp cao hơn.
  Cấp và bậc càng cao thì hiệu quả khởi trận càng cao.

* Để thăng bậc Kim Lan Trận cần tiêu hao điểm tình nghĩa
  (có được thông qua tổ đội với các bằng hữu kết bái kết thúc BOSS cuối ở các phụ bản) và lượng tiền vàng nhất định.

* Video giới thiệu nhiệm vụ tiến cấp Kim Lan Trận: https://www.youtube.com/watch?v=uMQRJ151tK0

Thống kê chi phí tiến cấp Kim Lan Trận
--------------------------------------

.. csv-table:: Thống kê chi phí tiến cấp Kim Lan Trận
   :header: "Cấp", "Chi tiết", "Tổng điểm tình nghĩa", "Tổng tiền vàng"

   0, 9 bậc - (150 tình nghĩa, 6v)/bậc, 9 x 150 = 1350, 9 x 4 = 36
   1, 9 bậc - (200 tình nghĩa, 6v)/bậc, 9 x 200 = 1800, 9 x 6 = 54
   2, 9 bậc - (270 tình nghĩa, 8v)/bậc, 9 x 270 = 2430, 9 x 8 = 72
   3,
   4
   5
   6
   7 9 x 500
   8 9 x 700
