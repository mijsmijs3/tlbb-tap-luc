Ẩn Hiện GUI Ingame
==================

* Mở tệp `System.cfg` trong thư mục `bin` được chứa trong thư mục cài đặt game (`<thư mục cài đặt game>/bin`),
  bằng Notepad++, Wordpad,.. và bổ sung giá trị sau:

  * EnableUIHiden =1
  * EnableSelfHiden =1
  * Lưu thay đổi `System.cfg`. Khởi động lại trò chơi, nhấn phím CapsLock/ScrollLock để ẩn giao diện GUI.

  .. image:: /tlbb_static/hinh_anh/thiet_lap/system_cfg.png

  .. image:: /tlbb_static/hinh_anh/thiet_lap/an_hien_gui_ingame.png
