Diệu Vũ Phương Lan
==================

Xuất hiện trong sự kiện `Lễ Hội Đón Xuân <http://ttl3d.zing.vn/su-kien/vu-khuc-mua-xuan/le-hoi-don-xuan.html>`_ (20/1/2016).

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phoi_suc/dieu_vu_phuong_lan/dieu_vu_phuong_lan_yeu.png
   :alt:  Diệu Vũ Phương Lan - Yêu

   Diệu Vũ Phương Lan - Yêu.

.. figure:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/phoi_suc/dieu_vu_phuong_lan/dieu_vu_phuong_lan_cuoc.png
   :alt:  Diệu Vũ Phương Lan - Cước

   Diệu Vũ Phương Lan - Cước.

