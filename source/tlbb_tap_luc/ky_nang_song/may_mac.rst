.. _ky-nang-song-may-mac:

May Mặc
=======

Học kỹ năng may mặc, xem :ref:`ky-nang-song-thang-cap-toi-da-truc-tiep`.

May mặc bằng nguyên liệu :ref:`ky-nang-song-trong-trot`, khi may phải đứng gần bàn may - Tô Châu (315, 165).

.. image:: /tlbb_static/hinh_anh/ky_nang_song/may_mac/may_mac_gan_ban_may_va.png

Cách thức chế đồ bằng nghề may mặc gồm 1 phần phụ, và 4 phần chinh:

* Thành phần phụ: 

  Cần phải có công thức để học cách chế tạo trang bị mong muốn, chỉ cần học một lần mà thôi.
  Cụ thể là phải có *Đồ tường* tương ứng, để tìm mua đồ tường, xem :ref:`ky-nang-song-do-tuong`.

* Thành phần chính:
  
  * **Đả tạo đồ** mà công thức may trang bị yêu cầu, để tìm mua Đả tạo đồ, xem :ref:`ky-nang-song-da-tao-do`.

  * **Vải dệt** mà công thức may trang bị yêu cầu, có được từ :ref:`ky-nang-song-trong-trot`.

  * **Nhận cách** có cấp độ mà công thức may trang bị yêu cầu, có được thông qua :ref:`ky-nang-song-gia-cong-nguyen-lieu`.

    .. figure:: /tlbb_static/hinh_anh/ky_nang_song/may_mac/gia_cong_nguyen_lieu_nhan_cach.png
       :alt: Gia công lông (mao) để nhận được nhận cách cấp tương ứng với cấp của vật phẩm lông (mao)

       Gia công *lông* (mao) để nhận được nhận cách cấp tương ứng với cấp của vật phẩm *lông* (mao).

    .. figure:: /tlbb_static/hinh_anh/ky_nang_song/may_mac/vi_du_vat_pham_long_mao.png
       :alt: Lông (mao) mua từ Tiệm điểm tặng trong tiền trang lạc Dương

       Lông (mao) có thể mua từ Tiệm điểm tặng trong tiền trang Lạc Dương, hoặc nhặt từ quái vật dạng thú ở các bản đồ có cấp độ luyện công mà vật phẩm lông (mao) chú thích.

  * **Bố phiến** có cấp độ mà trang bị yêu cầu, có được thông qua :ref:`ky-nang-song-gia-cong-nguyen-lieu`.

    Bố phiến chia làm 3 loại:

    * Sơ cấp: gia công từ các loại nguyên liệu may mặc đặc thù cấp 1 - 3.

    * Trung cấp: gia công từ các loại nguyên liệu may mặc đặc thù cấp 4 - 6.

    * Cao cấp: gia công từ các loại nguyên liệu may mặc đặc thù cấp 7 - 12.

    Các loại nguyên liệu may mặc đặc thù dùng để gia công thành **bố phiến** là các vật phẩm có chữ: 
    *Hoàng Cốt, Địa Tâm, Huyền Ty, Thiên Nhãn* ở cuối tên.

    .. figure:: /tlbb_static/hinh_anh/ky_nang_song/may_mac/vi_du_vat_pham_gia_cong_thanh_bo_phien.png
       :alt: Gia công nguyên liệu phòng cụ đặc thù thành bố phiến cấp tương ứng
    
       Các vật phẩm nguyên liệu phòng cụ đặc thù, có thể tìm hiểu và mua tại Tiệm điểm tặng trong Tiền trang Lạc Dương, hoặc nhặt từ quái vật ở các bản đồ có cấp độ luyện công mà vật phẩm lông (mao) chú thích.

       **Mẹo**: Ví dụ như công thức may mặc một món đồ cấp 9 nào đó yêu cầu Bố phiến cao cấp, bạn chỉ cần dùng nguyên liệu đặc thù cấp 7 là vẫn gia công được thành bố phiến cao cấp (không cần dùng tới cấp 9).

       Sẽ tiết kiệm hơn khi mua bằng điểm tặng hoặc tiết kiệm thời gian khi đánh quái ở bản đồ luyện công cấp thấp hơn.


  .. figure:: /tlbb_static/hinh_anh/ky_nang_song/may_mac/may_mac.png
     :alt: Chế đồ may mặc thủ công tiêu tốn rất nhiều công sức, nhưng cũng là cái thú tiêu khiển

     Cần phải có nguyên liệu dệt may từ nghề trồng trọt, đả tạo đồ, nhận cách, bố phiến tương ứng để thực hiện chế đồ.
     Nghề chế đồ thủ công rất lắm công phu, nhưng cũng là cái thú giết thời gian.


Kinh Nghiệm Chế Đồ
------------------




