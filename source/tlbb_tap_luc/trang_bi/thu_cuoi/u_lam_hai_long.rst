.. _thu-cuoi-u-lam-hai-long:

U Lam Hải Long
==============

Xuất hiện trong sự kiện `Thiên Long Địa Đồ <http://ttl3d.zing.vn/su-kien/thien-long-dia-do-thang-11/the-le-494.html>`_ (10/11/2015)
cùng :ref:`thu-cuoi-phi-thuy-ngu-gia`, :ref:`thu-cuoi-tu-mi-ung`.

.. image:: /tlbb_static/hinh_anh/trang_bi/thu_cuoi/u_lam_hai_long/hinh_anh.jpg
