Phụng Minh Vương Lăng
=====================

Phó bản hoạt động Phụng Minh Vương Lăng hay còn gọi là hoạt động Long Văn Phụng Minh Trấn.

* Tổ đội 3 người đẳng cấp >= 85
* Giới hạn trong ngày: 2 lần

Tiêu diệt 4 Man Hoang Dị Thú. 9 Dị Thú cả thảy trong hoạt động này, tuy nhiên mỗi vòng chỉ chọn ngẫu nhiên 4 dị thú lần lượt ra ứng chiến.

.. image:: /tlbb_static/hinh_anh/hoat_dong/phung_minh_vuong_lang/phung_minh_vuong_lang.jpg

Cách tham gia đánh phó bản hoạt động Phụng Minh Vương Lăng (Long Văn Phụng Minh Trấn):

#. **Anh Chiêu**

   .. image:: /tlbb_static/hinh_anh/hoat_dong/phung_minh_vuong_lang/boss_anh_chieu.jpg

   Kỹ năng đặc trưng:

   - Phù Sinh Nhược Mộng: Gây ra tổn thương thuộc tính huyền lớn đối với người chơi trong đội cách nó xa nhất.

   - Hồn Khiên Mộng Nhiễu: Kéo mọi người chơi trong phạm vi nhất điịnh về phía boss, đồng thời gây tổn hại thuộc tính huyền lớn cho họ.

   Khi BOSS thi triển hút cả đội về, sau vài giây BOSS sẽ tỏa sát thương giảm máu theo % (~ vài chục nghìn máu), khi BOSS hút lại hãy chạy ra xa tầm đánh của BOSS.

#. **Tương Liễu**

   .. image:: /tlbb_static/hinh_anh/hoat_dong/phung_minh_vuong_lang/boss_tuong_lieu.jpg

   Boss Tương Liễu không chỉ có công kích kịch độc hôi thối, mà còn triệu hồi những linh hồn thống khổ bị nó bắt giữ.

   Kỹ năng đặc trưng:

   - Hủ Độc Triền Thân: Thi triển với một người chơi trong đội ngũ. Khi trúng Hủ Độc Triền Thân, người chơi đó sẽ bị phong ấn, sau khi trạng thái phong ấn kết thúc sẽ gây tổn thương độc tính cho người chơi xung quanh.

   - Nhiếp Hồn Đại Trận: Xung quanh boss xuất hiện nhiều hồn phách, trong đó có hồn thật và hồn giả, mọi người cần tìm ra hồn thật và giết chết nó.

   Nhắc nhở: Sau khi boss thi triển Hủ Độc Triền Thân, mọi người cần cách xa chiến hữu bị ảnh hưởng bởi trạng thái đó. 
   Còn với Nhiếp Hồn Đại Trận, người chơi phải tập trung vào tìm hồn thật và giết nó, bởi vì boss lúc đó là vô địch rồi.   

#. **Thao Thiết**

   Boss Thao Thiết không chỉ có lực công kích khổng lồ kinh người, mà còn có Thị Ma Giả cung cấp lương thực và bổ sung lực chiến đấu cho nó.

   .. image:: /tlbb_static/hinh_anh/hoat_dong/phung_minh_vuong_lang/boss_thao_thiet.jpg

   Kỹ năng đặc trưng:

   - Thiên Sinh Thần Lực: Tạo ra tổn thương công kích ngoại lực lớn đối với một người chơi trong đội ngũ.

   - Tham Thực Tú Phương: Thao Thiết sau khi sử dụng kĩ năng này sẽ hô hào 04 Thị Ma Giả xuất hiện. Khi Thị Ma Giả xuất hiện, boss sẽ xuất chiêu tấn công thuộc tính hỏa trong phạm vi nhất định.

   Khi nhìn thấy 4 con Thị Ma Giả, phải giết nó ngay, để chúng sống càng lâu thì càng bị rút máu và BOSS lúc đó cũng miễn nhiễm sát thương.
   
#. **Cùng Kỳ**

   BOSS này trong lúc đánh sẽ gọi ra 2 con đệ tử phân thân, cấu máu đồng đội, và lúc đó BOSS sẽ miễn nhiễm sát thương.
   
   Cho nên hãy diệt 2 con phân thân càng nhanh càng tốt, nếu không nó sẽ đẻ thêm phân thân và rút máu dữ dội.
   
#. **Khai Minh**

   .. image:: /tlbb_static/hinh_anh/hoat_dong/phung_minh_vuong_lang/boss_khai_minh.jpg

   Lửa hỏa của Khai Minh thú một khi phun trào, nhất định khiến người chơi lúng ta lúng túng trong một thời gian.

   Kỹ năng đặc trưng:

   - Nộ Hỏa Xung Thiên: Tốc độ di chuyển của Khai Minh thú tăng cao, đồng thời trong phạm vi nhỏ giải phóng ra công kích thuộc tính hỏa.

   - Vô Tận Nộ Hỏa: Trong phụ bản giải phóng cạm bẫy, tạo tổn thương thuộc tính hỏa.

   Cách đánh là chia đều tổ đội đứng cách nhau và xa BOSS (theo hình vòng tròn, BOSS ở giữa).
   BOSS sẽ chỉ nhắm đến ai đứng cách xa nó nhất và lao tới húc (gây mất máu nghiêm trọng), trước khi hút sẽ có đủ thời gian để biết và khinh công né ra.
   
   Trong quá trình đánh, BOSS sẽ xuất một bẫy lửa trước, sau đó vài giây, BOSS sẽ xuất tiếp bẫy lửa vào vị trí mà người chơi đang đứng. Cách đánh là khi thấy BOSS tung bẫy thì chạy 2 lần để né 2 bẫy rồi đánh tiếp.

#. **Lục Ngô**

   BOSS này cũng tương tự như Khai Minh thú, nó sẽ thường xuyên thả bẫy băng, làm chậm tốc độ di chuyển sau vài giây sẽ phát nổ lan.
   Khi BOSS xuất bẫy, cả đội phải di chuyển ra khỏi ngay (khinh công, Lăng Ba Vi Bộ, Thế Vân Tung,...).
   
#. **Tất Phương**

   Ban đầu BOSS sẽ triệu hồi thêm 2 đệ tử giống y hệt mình, tổng cộng là có 3 con, nhưng chỉ có 1 con trong đó là BOSS thật.
   BOSS thật sẽ nói vài câu trước khi cả 3 di chuyển qua lại làm rối mắt.
   
   Nhiệm vụ là phải căng mắt nhìn xem đâu là BOSS thật và kết liễu nó để nhận thưởng.   
   
#. **Cổ Điêu**

   BOSS sẽ đi vòng tròn qua từng cột thiên lôi, canh sao sát thương vừa đủ để khi BOSS đứng trong trụ thiên lôi thì đúng lúc trụ phát nổ.   
   
#. **Trường Hữu**

   Chỉ cần tiêu diệt thức ăn thơm ngon, để thức ăn hỏng cho Trường Hữu ăn càng nhiều thì BOSS sẽ nhanh chết.


Có thể nhận xét, 9 BOSS trong phó bản hoạt động Phụng Minh Vương Lăng (Long Văn Phụng Minh Trấn) chia làm một số loại chính sau:

* Phân thân, bất tử, hút máu.
* Thả bẫy.

*Nguồn, tham khảo, trích dẫn: kinh nghiệm cá nhân, hình ảnh từ VietGiaiTri, TTL3DZING*