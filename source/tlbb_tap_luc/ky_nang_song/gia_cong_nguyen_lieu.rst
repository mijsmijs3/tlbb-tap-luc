.. _ky-nang-song-gia-cong-nguyen-lieu:

Gia Công Nguyên Liệu
====================

Gia công một số nguyên liệu thành phần cho các kỹ năng :ref:`ky-nang-song-may-mac`, Công nghệ, Đúc. Điểm đặc biệt là không tiêu hao hoạt lực khi thực hiện gia công.

.. image:: /tlbb_static/hinh_anh/ky_nang_song/gia_cong_nguyen_lieu/ky_nang.png

Học kỹ năng Gia công nguyên liệu tại NPC Phùng Chú Thiết - Lạc Dương (250, 313).
Chỉ học một lần không cần thăng cấp, có thể gia công tất cả nguyên liệu từ sơ cấp đến trung cấp, cao cấp.

.. image:: /tlbb_static/hinh_anh/ky_nang_song/gia_cong_nguyen_lieu/hoc_ky_nang.png


Nguyên liệu đặc thù để gia công có thể mua từ Tiệm điểm tặng trong tiền trang Lạc Dương, hoặc nhặt từ quái vật ở các bản đồ luyện cấp và nguyên liệu có chú thích.

.. image:: /tlbb_static/hinh_anh/ky_nang_song/gia_cong_nguyen_lieu/tiem_diem_tang.png




