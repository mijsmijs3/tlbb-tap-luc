.. _thu-cuoi-bich-giap-huyen-quy:

Bích Giáp Huyền Quy
===================

* Xuất hiện trong sự kiện
  `Báo Danh Năng Động <http://ttl3d.zing.vn/su-kien/hiep-khach-giang-ho/bao-danh-nang-dong-440.html>`_ (9/9/2015)
  cùng thời trang :ref:`thoi-trang-bich-huy-ham-phuong`.

  .. image:: /tlbb_static/hinh_anh/trang_bi/thu_cuoi/bich_giap_huyen_quy/hinh_anh.jpg