.. _thoi-trang-da-cam-thien-lan:

Dạ Cẩm Thiên Lan
================

Xuất hiện ban đầu trong chuỗi sự kiện `Trung Thu Dạ Yến <http://ttl3d.zing.vn/su-kien/trung-thu-da-yen/thoi-trang-thu-moi.html>`_ (23/9/2015).

Xuất hiện trong sự kiện `Thiên Long Thưởng Nguyệt <http://ttl3d.zing.vn/su-kien/thien-long-thuong-nguyet/huong-dan.html>`_ (25/9/2015).

.. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/da_cam_thien_lan/hinh_anh.jpg

.. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/da_cam_thien_lan/vat_pham.jpg

.. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/da_cam_thien_lan/cap_doi_1.jpg

.. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/da_cam_thien_lan/cap_doi_2.jpg

.. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/da_cam_thien_lan/cap_doi_3.jpg