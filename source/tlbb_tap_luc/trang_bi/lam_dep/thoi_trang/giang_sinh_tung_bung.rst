Giáng Sinh Tưng Bừng
====================

* | Xuất hiện trong sự kiện `Thắp Đèn Đông Phúc <http://ttl3d.zing.vn/su-kien/thap-den-dong-phuc/phan-thuong-532.html>`_ 23/12/2015
  | Tham gia thắp đèn hoàn thành 30 ô, có cơ hội nhận được.

  .. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/giang_sinh_tung_bung/vat_pham.png
