Bàn Cổ Chi Linh
===============

*Nhắc lại*: Bàn Cổ Chi Linh - Một trong những hoạt động được ra mắt vào gần cuối thời điểm chấm dứt vận hành game Thiên Long Bát Bộ của FPT.

* Mỗi thứ 5 và Chủ nhật hằng tuần, hoạt động được mở ra vào lúc 20h30.
* Nhân sĩ tham gia đẳng cấp >= 75, tiêu diệt 20 con Xa Hình Nê Nhân và đợi 3 phút sau BOSS sẽ xuất hiện.

Bàn Cổ Chi Linh - Địa
---------------------

* Thời gian: 20h30 - Thứ 5 hằng tuần.
* Địa điểm: **Thái Hồ**.

Bàn Cổ Chi Linh - Thiên
-----------------------

* Thời gian: 20h30 - Chủ nhật hằng tuần.
* Địa điểm: **Dạ Tây Hồ** (Từ Tây Hồ đi vào, *đây là cảnh Tây Hồ về đêm, có thể bắt đom đóm ở đây*).

.. figure:: /tlbb_static/hinh_anh/hoat_dong/ban_co_chi_linh/ban_co_chi_linh_thien_1.*
     :alt: Thời gian và địa điểm BOSS Bàn Cổ Chi Linh - Thiên

     Thời gian và địa điểm BOSS Bàn Cổ Chi Linh - Thiên

Tham gia
--------

* Tham gia gây sát thương lên BOSS, sát thương sẽ được tính ra phần trăm (%) máu của BOSS, tương ứng số % này sẽ là các phần thưởng hoạt động khác nhau.
* Một cách đánh BOSS dành cho những bạn có nhân vật không quá mạnh (~ bán full 4), và muốn gây sát thương >= 1% máu BOSS (phần thưởng khi đánh được 1% máu BOSS sẽ nói ở dưới):
    * Quan sát xem máy chủ bạn đánh có đông không, nếu vừa phải (BOSS chết trong tầm 20-30 phút sau khi xuất hiện) thì bạn có khả năng tham gia đấy.

    * Phải đánh ngay lúc BOSS vừa xuất hiện, trong vài phút đầu thì sát thương bạn gây lên BOSS sẽ như đánh quái vật bình thường (vài chục k sát thương trở lên), sau vài phút BOSS trở chứng và tự buff hiệu ứng miễn dịch làm giảm sát thương bạn gây ra đáng kể (còn vài k sát thương + sát thương xuyên thích).

    * Chuẩn bị một con pet ngoại công, vì BOSS sẽ khá thường xuyên tự buff hiệu ứng miễn dịch sát thương nội công (các phái nội chịu thiệt), nên lúc BOSS buff hiệu ứng này ra thì bạn triệu hồi pet ngoại công cho BOSS hưởng trọn sát thương của con pet.

    * Quan sát và chủ động né các hiệu ứng của BOSS sau đây (trước khi buff hiệu ứng, BOSS sẽ hô lên (kênh chat gần + hệ thống):
        * Bán Sinh Bán Tử: khoảng một nửa số nhân vật đứng cạnh BOSS sẽ bị giảm 50% máu.
        * Thiên Cang Bá Khí: ngưng đánh BOSS vì lúc này sẽ phản đòn sát thương (giống con Lư Quân Dật cưỡi ngựa trong phụ bản Sát tinh), các bạn chú ý quan sát cảnh vật nếu thấy trên người con BOSS không còn luồng sáng màu vàng xoay vòng tròn nữa thì hẵng đánh tiếp. Khi gặp các hiệu ứng này, nếu được thì hãy chạy hoặc dịch chuyển ra xa BOSS.

    * Thường xuyên dùng bí tịch có gây sát thương trực tiếp như Vân Hải Nghênh Khách Phật Tông, ... Nga Mi thì nhớ siêng xài bí tịch Nga mi 65 Cửu Âm Thần Trảo.

    * Cố gắng cường hóa các trang bị tấn công để tăng sát thương xuyên thích.

.. figure:: /tlbb_static/hinh_anh/hoat_dong/ban_co_chi_linh/ban_co_chi_linh_thien_2.*
     :alt: BOSS Bàn Cổ Chi Linh - Thiên

     BOSS Bàn Cổ Chi Linh - Thiên

Phần Thưởng
-----------

Chỉ cần gây 1 điểm sát thương trở lên là bạn chắc chắn có phần thưởng.

* 1 điểm sát thương (1 chưởng vào BOSS): Kim tàm ti x1, Vàng tài phú x1 (bán được 40 vàng khóa).
* 1% máu BOSS: Chí Tôn Cường Hóa Tinh Hoa x1, Chân Nguyên Tinh Phách x1, Bí pháp kinh mạch x1, Bảo Thạch cấp 2 x1, danh hiệu 7 ngày Bàn Cổ Dũng Sĩ.
* TOP 10: Thú cưỡi Xích Vân Diễm Long 7 ngày, Chí Tôn Cường Hóa Tinh Hoa x1, BUFF hiệu ứng khống chế 7 ngày.
* TOP 3: Thú cưỡi Xích Vân Diễm Long 7 ngày, danh hiệu Bàn Cổ Chi Linh Dũng Cảm/Thám Hoa/Bảng Nhãn 7 ngày, BUFF hiệu ứng giảm % máu 7 ngày.
* Tuyệt sát (đòn đánh chí tử cuối cùng): Thú cưỡi Xích Vân Diễm Long 7 ngày, Chân Nguyên Tinh Phách x1, Bí pháp kinh mạch x1, danh hiệu Bàn Cổ Tuyệt Sát 7 ngày.

Ngoài ra phần thưởng nào cũng đi kèm Bàn Cổ thạch (Thiên hoặc Địa) x1.

Thông tin phần thưởng còn thiếu sót, bạn nào biết thì bình luận rõ ràng hơn ở bên dưới nhé. Cảm ơn!
Cảm ơn IGiaHanI - Giao Long 7 đã cung cấp một số thông tin!

Danh hiệu
---------

Dùng Bàn Cổ Thạch để đổi danh hiệu vĩnh viễn, được chia nhóm theo 2 BOSS Bàn Cổ Chi Linh - Thiên và Bàn Cổ Chi Linh - Địa:

* Bàn Cổ Chi Linh - Địa (Danh hiệu **Kỳ Tài**: tăng chính xác, sát thương thuộc tính Băng Công, Hỏa Công, Huyền Công, Độc Công)
    #. Kỳ Tài Tiếng Tăm: Bàn Cổ Địa Thạch x1
    #. Kỳ Tài Vang Danh Một Thời: Bàn Cổ Địa Thạch x5
    #. Kỳ Tài Rất Mực Tao Nhã: Bàn Cổ Địa Thạch x10
    #. Kỳ Tài Số Một: Bàn Cổ Địa Thạch x20
    #. Kỳ Tài Độc Nhất Vô Nhị: Bàn Cổ Địa Thạch x50

* Bàn Cổ Chi Linh - Thiên (Danh hiệu **Thiên Tài**: tăng tất cả thuộc tính (tiềm năng), giới hạn sinh lực)
    #. Thiên Tài Mới Nổi: Bàn Cổ Thiên Thạch x1
    #. Thiên Tài Oai Phong: Bàn Cổ Thiên Thạch x5
    #. Thiên Tài Tiếu Ngạo Giang Hồ: Bàn Cổ Thiên Thạch x10
    #. Thiên Tài Lưu Danh Muôn Đời: Bàn Cổ Thiên Thạch x20
    #. Thiên Tài Khoáng Thế Thần Thoại: Bàn Cổ Thiên Thạch x50

Biên soạn: LingYun - Giao Long 7







