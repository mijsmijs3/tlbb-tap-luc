.. _thoi-trang-cam-tu-nien-hoa:

Cẩm Tú Niên Hoa
===============

* Xuất hiện trong sự kiện
  `Báo Danh Năng Động <http://ttl3d.zing.vn/su-kien/mua-tuyet-vui-ve/bao-danh-nang-dong-516.html>`_ (9/12/2015)
  cùng thú cưỡi :ref:`thu-cuoi-cu-vien`.

.. image:: /tlbb_static/hinh_anh/trang_bi/lam_dep/thoi_trang/cam_tu_nien_hoa/cap_doi.jpg
