.. _ky-nang-song-trong-trot:

Trồng Trọt
==========

Phân bố tài nguyên trồng trọt tại bãi người rơm ở các bản đồ thành thị.
Khi trồng trọt sẽ tiêu hao một lượng tinh lực tùy theo cấp độ sản phẩm.
  
* ĐL: Đại Lý
* LD: Lạc Dương
* TC: Tô Châu
* LL: Lâu Lan

============== ========== ============ ========== ========== ========
Cấp trồng trọt Sản phẩm   Bạn sinh     Loại       Vị trí     Tên dịch
============== ========== ============ ========== ========== ========
1              Trữ ma                  Dệt        ĐL, LD, TC Cây gai
2              Thảo miên               Dệt        LD         Bông cỏ
3              Á ma                    Dệt        TC         Cây lanh
4              Mộc miên                Dệt        ĐL         Cây gạo
5              Hoàng ma                Dệt        LD         Cây đay
6              Vân miên                Dệt        TC         Gấm hoa
7              Cẩn ma                  Dệt        ĐL, LL     Đay cẩn
8              Nhung miên              Dệt        LD, LL     Gấm nhung
9              Thanh ma                Dệt        TC, LL     Đay
10             Thái miên               Dệt        TC, LL     Thái miên
1              Tiểu mạch  Hoa tiêu     Lương thực ĐL, LD, TC Lúa mì
2              Đại mễ     Bạc hà       Lương thực ĐL, TC     Gạo
3              Ngọc mễ    Hồ tiêu      Lương thực LD         Ngô
4              Hoa sinh   Bát giác     Lương thực LD         Đậu phụng
5              Hồng thự   Sinh thái    Lương thực ĐL, TC     Khoai lang
6              Cao lương  Đại toán     Lương thực LD         Cao lương
7              Chi ma     Giới mạt     Lương thực ĐL, LL     Mè
8              Lục đậu    Hoàng khương Lương thực TC, LL     Đậu xanh
9              Hoàng đậu  Thanh thông  Lương thực ĐL, TC, LL Đậu nành
10             Tàm đậu    Hồi hương    Lương thực LD, TC, LL Đậu tằm
============== ========== ============ ========== ========== ========

* Tham khảo: k2namanh
