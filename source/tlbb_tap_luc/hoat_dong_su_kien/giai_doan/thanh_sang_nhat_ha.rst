Thanh Sảng Nhất Hạ
==================

Tên gọi truyền thiết là "hái dưa", cứ mỗi mùa hè game thủ TLBB đều trông chờ vào sự kiện này, vì ai cũng có thể tham gia, phần thưởng dành cho người chịu khó siêng năng cũng rất ổn - Kim Tàm Ty để nâng cấp điêu văn trang bị, Căn cốt đan & Linh thú tinh phách để chăm sóc và làm trân thú.

May mắn còn có Tiềm Năng Quả, nhưng Lam Diệp Thảo lại còn hiếm hơn.

Từ lúc trò chơi về tay NPH VNG, hoạt động này được mở sớm hơn nhiều so với trước đây (phải gọi là tận trước hè).

* `Hè năm 2017 <http://ttl3d.zing.vn/su-kien/cap-nhat-tuan-12-04-2017/thanh-sang-nhat-ha-1056.html>`_

* `Hè năm 2016 <http://ttl3d.zing.vn/su-kien/mua-he-tuoi-mat/thanh-sang-nhat-ha.html>`_