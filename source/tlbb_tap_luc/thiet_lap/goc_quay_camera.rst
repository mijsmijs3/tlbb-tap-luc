Độ Cao Góc Quay Trong Game
==========================

* Mở tệp `System.cfg` trong thư mục `bin` được chứa trong thư mục cài đặt game (`<thư mục cài đặt game>/bin`),
  bằng Notepad++, Wordpad,.. và thay đổi (nâng) giá trị sau:

  * *Camera_MaxDistance*

    VD: Camera_MaxDistance = 60.0
  * Lưu thay đổi `System.cfg`. Khởi động lại trò chơi, bấm ESC > Hệ thống > Hiển thị > Bỏ chọn `Góc nhìn chuẩn`

    System.cfg

    .. image:: /tlbb_static/hinh_anh/thiet_lap/system_cfg.png

    Các thông số có thể điều chỉnh như trong hình

    .. image:: /tlbb_static/hinh_anh/thiet_lap/thong_so_goc_nhin_camera.png
