.. _thu-cuoi-cu-vien:

Cự Viên
=======

Xuất hiện trong sự kiện
`Báo Danh Năng Động <http://ttl3d.zing.vn/su-kien/mua-tuyet-vui-ve/bao-danh-nang-dong-516.html>`_ (9/12/2015)

.. figure:: /tlbb_static/hinh_anh/trang_bi/thu_cuoi/cu_vien/vat_pham.png
   :alt: Hình ảnh vật phẩm thú cưỡi Cự Viên có được từ sự kiện Báo Danh Năng Động

.. figure:: /tlbb_static/hinh_anh/trang_bi/thu_cuoi/cu_vien/hinh_anh.png
   :alt: Hình ảnh khi cưỡi Cự Viên rất hầm hố, khổng lồ

   Hình ảnh khi cưỡi Cự Viên rất hầm hố, khổng lồ.

